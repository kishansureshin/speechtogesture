package com.example.androidchatclient;

import java.util.Arrays;
import java.util.List;

//import com.example.speechtotext.R;
//import com.example.speechtotext.SendMailActivity;

//import com.example.speechtotext.R;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class ShowGesture extends Activity {
	
	int flag=0;
//	Button sendMail;
	EditText recepientAddress,messageContent;
	int []imageArray={R.drawable.ab,R.drawable.b,R.drawable.c,R.drawable.d};
	ImageView im;
	List<String> files = Arrays.asList("a","b","c","d","e","f","g","h","hello","good","morning");
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mail);
//		sendMail = (Button) findViewById(R.id.btn_send_email);
//		recepientAddress = (EditText) findViewById(R.id.et_recepient);
		messageContent = (EditText) findViewById(R.id.et_message);
		im = (ImageView) findViewById(R.id.imageView1);
		Bundle bundle = getIntent().getExtras();
		String message = bundle.getString("mytext");
		messageContent.setText(message);
		final String[] words = message.split("\\s+");
		

		
		for(String word: words){
			String replacedStr = word.replace("\\", "");
            if(!files.contains(replacedStr)){
            	flag=1;
            }
		}
		if(flag == 1) {
			Toast.makeText(ShowGesture.this,"some words not found in db", Toast.LENGTH_SHORT).show();
		}	
        else
        {
    		final Handler handler = new Handler();
    	    Runnable runnable = new Runnable() {
    	       int i=0;
    	       public void run() {
    	    	   String uri = "@drawable/"+words[i];
    	    	   int imageResource = getResources().getIdentifier(uri, null, getPackageName());
    	    	   Drawable res = getResources().getDrawable(imageResource);
    	    	   im.setImageDrawable(res);
//    	           im.setImageResource(imageArray[i]);
    	           i++;
    	           if(i>words.length-1) {
    	               i=0;    
    	           }
    	           handler.postDelayed(this, 500);  //for interval...
    	       }
    	   };
    	   handler.postDelayed(runnable, 2000);
        }
	}
	
	
}
